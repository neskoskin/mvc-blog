﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcBlog.Models;

namespace MvcBlog.Controllers
{ 
    public class BlogPostController : Controller
    {
        private Blogger db = new Blogger();

        //
        // GET: /BlogPost/

        public ViewResult Index()
        {
            var blogpost = db.BlogPost.Include(b => b.Blog);
            return View(blogpost.ToList());
        }

        //
        // GET: /BlogPost/Details/5

        public ViewResult Details(int id)
        {
            BlogPost blogpost = db.BlogPost.Find(id);
            return View(blogpost);
        }

        //
        // GET: /BlogPost/Create

        public ActionResult Create()
        {
            ViewBag.BlogID = new SelectList(db.Blog, "BlogID", "BlogTitle");
            return View();
        } 

        //
        // POST: /BlogPost/Create

        [HttpPost]
        public ActionResult Create(BlogPost blogpost)
        {
            if (ModelState.IsValid)
            {
                db.BlogPost.Add(blogpost);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.BlogID = new SelectList(db.Blog, "BlogID", "BlogTitle", blogpost.BlogID);
            return View(blogpost);
        }
        
        //
        // GET: /BlogPost/Edit/5
 
        public ActionResult Edit(int id)
        {
            BlogPost blogpost = db.BlogPost.Find(id);
            ViewBag.BlogID = new SelectList(db.Blog, "BlogID", "BlogTitle", blogpost.BlogID);
            return View(blogpost);
        }

        //
        // POST: /BlogPost/Edit/5

        [HttpPost]
        public ActionResult Edit(BlogPost blogpost)
        {
            if (ModelState.IsValid)
            {
                db.Entry(blogpost).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BlogID = new SelectList(db.Blog, "BlogID", "BlogTitle", blogpost.BlogID);
            return View(blogpost);
        }

        //
        // GET: /BlogPost/Delete/5
 
        public ActionResult Delete(int id)
        {
            BlogPost blogpost = db.BlogPost.Find(id);
            return View(blogpost);
        }

        //
        // POST: /BlogPost/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            BlogPost blogpost = db.BlogPost.Find(id);
            db.BlogPost.Remove(blogpost);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}