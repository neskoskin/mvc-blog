﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcBlog.Models;
using System.Web.Security;
namespace MvcBlog.Controllers
{
    public class UserController : Controller
    {
        Blogger blogger = new Blogger();

        //
        // GET: /User/
        public ActionResult Index()
        {
         return View();
        }
        //
        //GET: /User/Create/
        public ActionResult Create()
        {
            return View();
        
        }
        //
        // POST: /User/Create
        [HttpPost]
        public ActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                blogger.User.Add(user);
                blogger.SaveChanges();
                ViewBag.Message = "You are registered";
                return RedirectToAction("Blog");
               
            }
            return View(user);
        }
        //
        // GET: /User/Login
        public ActionResult Login()
        {

            return View();
        }
        //
        // POST: /User/Login
        [HttpPost]
        public ActionResult Login(Login login)
        {
            if (ModelState.IsValid)
            {
                var count = (from usr in blogger.User
                             where usr.UserName == login.UserName && usr.Password == login.Password
                             select usr).ToList().Count;
                if (count == 1)
                {
                    FormsAuthentication.SetAuthCookie(login.UserName, true);
                    User currentUser = (from usr in blogger.User
                                        where usr.UserName == login.UserName && usr.Password == login.Password
                                        select usr).FirstOrDefault();

                    if (currentUser.Blog.Count > 0)
                    {
                        return RedirectToAction("BlogPost");
                    }
                    else
                    {
                        return RedirectToAction("Blog");
                    }
                }
                else
                {
                    ViewBag.Message = "UserName or Password is wrong";

                }
            }
            return View();

        }
        //
        // GET: /User/Blog
        public ActionResult Blog(User user)
        {
            ViewBag.Message = User.Identity.Name.ToString();
            return View();
        
        }
        //
        // POST: /User/Blog
        [HttpPost]
        public ActionResult Blog(Blog blog)

        {
            if (ModelState.IsValid){
                User loggedInUser = (from usr in blogger.User
                      where usr.UserName == User.Identity.Name
                      select usr).FirstOrDefault();
                blog.UserID = loggedInUser.UserID;
                blogger.Blog.Add(blog);
                blogger.SaveChanges();
                ViewBag.Message = "You have succesfully named your blog";
            return RedirectToAction("BlogPost");
            }
            return View();
        }
        //
        // GET: /User/BlogPost
        public ActionResult BlogPost(Blog blog)
        {
            return View();
        
        }
        //
        // POST: /User/BlogPost
        [HttpPost]
        public ActionResult BlogPost(BlogPost blogpost)
        {
                if (ModelState.IsValid){
                User loggedInUser = (from usr in blogger.User
                      where usr.UserName == User.Identity.Name
                      select usr).FirstOrDefault();
                blogpost.BlogID = loggedInUser.Blog.First().BlogID;
                blogger.BlogPost.Add(blogpost);
                blogger.SaveChanges();
                ViewBag.Message = "You have succesfully added a new post to your blog";
            }
            return View(blogpost);
        }
        //
        // GET: /User/ViewBlog

    }
}
