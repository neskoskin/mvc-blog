﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MvcBlog.Models
{
    public class Login
    {
        [Required(ErrorMessage = "Please enter a UserName")]
        [StringLength(20, ErrorMessage = "UserName too long")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Please enter a password")]
        [StringLength(30, ErrorMessage = "Password too long")]
        public string Password { get; set; }
    }
}