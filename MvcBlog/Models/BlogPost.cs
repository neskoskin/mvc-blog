﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace MvcBlog.Models
{
    public class BlogPost
    {
        [Key]
        public int PostID { get; set; }
        public int BlogID { get; set; }
        [Required(ErrorMessage = "Please enter a Post Title")]
        [StringLength(20, ErrorMessage = "Title Name too long")]
        public string PostTitle { get; set; }
        [Required(ErrorMessage = "Please enter a Post Content")]
        [StringLength(200, ErrorMessage = "Title Name too long")]
        public string PostContent { get; set; }
        public DateTime PostTime { get; set; }
        public virtual Blog Blog { get; set; }
    }
}