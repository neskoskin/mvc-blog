﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MvcBlog.Models
{
    public class Blog
    {
        [Key]
        public int BlogID { get; set; }
        public int UserID { get; set; }
        [Required(ErrorMessage = "Please enter a Blog Title")]
        [StringLength(20, ErrorMessage = "Blog Name too long")]
        public string BlogTitle { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<BlogPost> BlogPost { get; set; }
    }
}